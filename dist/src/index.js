"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const challenge_router_1 = __importDefault(require("./routes/challenge_router"));
const app = express_1.default();
// settings
app.set('port', process.env.PORT || 3000);
// middlewares
app.use(express_1.default.json());
app.use(express_1.default.urlencoded({ extended: false }));
// routes
app.use('/evaluation', challenge_router_1.default);
app.listen(app.get('port'), () => {
    console.log(`Server on port ${app.get('port')}`);
});
module.exports = app;
