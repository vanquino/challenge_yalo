"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.togglService = void 0;
const node_fetch_1 = __importDefault(require("node-fetch"));
class TogglService {
    getWorkspaces() {
        return new Promise((resolve, reject) => {
            // fetch('https://www.toggl.com/api/v8/time_entries', {
            // fetch('https://www.toggl.com/api/v8/workspaces/4330393/tasks', {
            //fetch('https://www.toggl.com/api/v8/workspaces/4330393/projects', { OK
            node_fetch_1.default('https://www.toggl.com/api/v8/time_entries', {
                //https://www.toggl.com/api/v8/time_entries/current
                //https://www.toggl.com/api/v8/time_entries?start_date=2020-01-01&end_date=2020-12-31
                //https://www.toggl.com/api/v8/workspaces/4330393/projects
                // fetch('https://www.toggl.com/api/v8/workspaces/4330393', {
                method: 'GET',
                headers: {
                    'Authorization': `Basic ${Buffer.from('e4fdc768834404305f5b3e341c4bf0cb:api_token')
                        .toString('base64')}`,
                    'Accept': 'application/json'
                }
            })
                .then(result => resolve(result.json()))
                .catch(error => reject(error));
        });
    }
}
exports.togglService = new TogglService();
