"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.jiraService = void 0;
const node_fetch_1 = __importDefault(require("node-fetch"));
class JiraService {
    getProjectByKey(key) {
        return new Promise((resolve, reject) => {
            node_fetch_1.default(`https://stackitchallenge.atlassian.net/rest/api/3/project/${key}`, {
                method: 'GET',
                headers: {
                    'Authorization': `Basic ${Buffer.from('victor.anquino@outlook.com:S8I6XjKqdRuvEnRXxshEA956')
                        .toString('base64')}`,
                    'Accept': 'application/json'
                }
            })
                .then(result => resolve(result.json()))
                .catch(error => reject(error));
        });
    }
    getProjects() {
        return new Promise((resolve, reject) => {
            node_fetch_1.default('https://stackitchallenge.atlassian.net/rest/api/3/project/', {
                method: 'GET',
                headers: {
                    'Authorization': `Basic ${Buffer.from('victor.anquino@outlook.com:S8I6XjKqdRuvEnRXxshEA956')
                        .toString('base64')}`,
                    'Accept': 'application/json'
                }
            })
                .then(result => resolve(result.json()))
                .catch(error => reject(error));
        });
    }
    getIssuesByProjectId(projectId) {
        return new Promise((resolve, reject) => {
            node_fetch_1.default(`https://stackitchallenge.atlassian.net/rest/api/2/search?jql=project=${projectId}`, {
                method: 'GET',
                headers: {
                    'Authorization': `Basic ${Buffer.from('victor.anquino@outlook.com:S8I6XjKqdRuvEnRXxshEA956')
                        .toString('base64')}`,
                    'Accept': 'application/json'
                }
            })
                .then(result => resolve(result.json()))
                .catch(error => reject(error));
        });
    }
}
exports.jiraService = new JiraService();
