"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.arithmetic = exports.logic = void 0;
class Logic {
    constructor() {
        this.operators = [];
        this.separators = [];
        this.operators = [">", "=", "<", "!", "|", "&"];
        this.separators = ["(", ")"];
    }
    evaluate(expression, context) {
        let str = this.sanitizer(expression, context);
        let validChar = str.split("");
        if (this.validString(validChar)) {
            let opened = false;
            let closed = false;
            let num1 = "";
            let num2 = "";
            let op = "";
            let lastType = "";
            let sepOpened = false;
            let sepClosed = false;
            let bool1 = "";
            let bool2 = "";
            while (this.isOperable(str)) {
                let characters = str.split("");
                characters.forEach((char, key) => {
                    if (this.separators.includes(char)) {
                        if (char == "(") {
                            sepOpened = true;
                            opened = true;
                        }
                        if (char == ")") {
                            sepClosed = true;
                            closed = true;
                        }
                        lastType = "separator";
                    }
                    if (this.operators.includes(char)) {
                        lastType = "operator";
                        let prev = (key > 0) ? characters[key - 1] : "";
                        let next = characters[key + 1];
                        if (!this.operators.includes(prev))
                            op += char;
                        if (this.operators.includes(next)) {
                            op += next;
                            return;
                        }
                    }
                    if (!isNaN(Number(char))) { // si el caracter actual es un numero
                        if (num1 == "") { // si no hay primer numero aún
                            num1 += char;
                            opened = true;
                        }
                        else { // numero1 no está vacio (1)
                            if (lastType == "number") { //si el ultimo caracter fue un numero
                                if (num2 == "") // si el numero 2 esta vacio aun
                                    num1 += char;
                                else // quiere decir que estoy armando el numero 2
                                    num2 += char;
                            }
                            if (lastType == "operator") { // si el ultimo caracter fue un operador
                                num2 += char;
                            }
                        }
                        lastType = "number";
                    }
                    if (!this.operators.includes(char) && !this.separators.includes(char) && isNaN(Number(char))) {
                        if (["true", "false"].includes(bool1)) {
                            if (["true", "false"].includes(bool2)) {
                            }
                            else {
                                bool2 += char;
                            }
                        }
                        else {
                            bool1 += char;
                            return;
                        }
                    }
                    if (key == characters.length - 1) { // si el caracter actual es el ultimo
                        closed = true;
                    }
                    else {
                        let next = characters[key + 1];
                        if (next == ')') {
                            sepClosed = true;
                            closed = true;
                        }
                        if (num2 == "" && (next == '(')) { //valida si el numero1 está solo (no tiene operación inmediata)
                            closed = true;
                        }
                    }
                    if (num1 && op && num2 && closed) {
                        let operation = num1 + op + num2;
                        let result = this.operate(num1, op, num2).toString();
                        if (sepOpened && sepClosed)
                            operation = "(" + operation + ")";
                        str = str.replace(operation, result);
                    }
                    if (["true", "false"].includes(bool1) && op && ["true", "false"].includes(bool2)) {
                        let operation = bool1 + op + bool2;
                        let result = this.operate(bool1, op, bool2).toString();
                        if (sepOpened && sepClosed)
                            operation = "(" + operation + ")";
                        str = str.replace(operation, result);
                    }
                    if (closed) {
                        num1 = "";
                        num2 = "";
                        bool1 = "";
                        bool2 = "";
                        op = "";
                        lastType = "";
                        opened = false;
                        closed = false;
                        sepOpened = false;
                        sepClosed = false;
                        return;
                    }
                });
            }
            return str;
        }
        return;
    }
    isOperable(str) {
        let characters = str.split("");
        let result = false;
        characters.forEach((char) => {
            //if( this.operators.includes(char) || this.separators.includes(char) ) {
            if (this.operators.includes(char)) {
                return result = true;
            }
        });
        return result;
    }
    operate(var1, operator, var2) {
        if (["true", "false"].includes(var1) && ["true", "false"].includes(var2)) {
            return this.compare(var1, var2, operator);
        }
        else {
            return this.compare(parseFloat(var1), parseFloat(var2), operator);
        }
    }
    compare(var1, var2, operator) {
        switch (operator) {
            case '==':
                try {
                    return var1 == var2;
                }
                catch (error) {
                    throw error;
                }
            case '>':
                try {
                    return var1 > var2;
                }
                catch (error) {
                    throw error;
                }
            case '<':
                try {
                    return var1 < var2;
                }
                catch (error) {
                    throw error;
                }
            case '>=':
                try {
                    return var1 >= var2;
                }
                catch (error) {
                    throw error;
                }
            case '<=':
                try {
                    return var1 <= var2;
                }
                catch (error) {
                    throw error;
                }
            case '!=':
                try {
                    return var1 != var2;
                }
                catch (error) {
                    throw error;
                }
            case '||':
                try {
                    return var1 || var2;
                }
                catch (error) {
                    throw error;
                }
            case '&&':
                try {
                    return var1 && var2;
                }
                catch (error) {
                    throw error;
                }
            default:
                return false;
        }
    }
    sanitizer(expression, context) {
        let result = "";
        expression = expression.replace(/ /g, "");
        if (context) {
            result = this.replaceVariables(expression, context);
        }
        else {
            result = expression;
        }
        return result;
    }
    validString(characters) {
        let valid = true;
        characters.forEach(char => {
            if (!this.validCharacter(char)) {
                return valid = false;
            }
        });
        return valid;
    }
    validCharacter(char) {
        if (this.separators.includes(char))
            return true;
        if (this.operators.includes(char))
            return true;
        if (!isNaN(Number(char)))
            return true;
        if (char == ".")
            return true;
        return false;
    }
    replaceVariables(expression, context) {
        let variables = Object.keys(context);
        variables.forEach(variable => {
            let value = context[variable];
            let term = new RegExp(variable, 'g');
            expression = expression.replace(term, value);
        });
        return expression;
    }
}
exports.logic = new Logic();
class Arithmetic {
}
exports.arithmetic = new Arithmetic();
