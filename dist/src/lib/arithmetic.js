"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.arithmetic = void 0;
class Arithmetic {
    constructor() {
        this.operators = [];
        this.separators = [];
        this.operators = ["+", "-", "*", "/", "**", "!"];
        this.separators = ["(", ")"];
    }
    evaluate(expression, context) {
        let str = this.sanitizer(expression, context);
        let validChar = str.split("");
        if (this.validString(validChar)) {
            let opened = false;
            let closed = false;
            let num1 = "";
            let num2 = "";
            let op = "";
            let lastType = "";
            let sepOpened = false;
            let sepClosed = false;
            let isFactorial = false;
            while (this.isOperable(str)) {
                let characters = str.split("");
                characters.forEach((char, key) => {
                    if (this.separators.includes(char)) {
                        if (char == "(") {
                            sepOpened = true;
                            opened = true;
                        }
                        if (char == ")") {
                            closed = true;
                        }
                        lastType = "separator";
                    }
                    if (this.operators.includes(char)) {
                        op += char;
                        if (char == '!') {
                            isFactorial = true;
                        }
                        lastType = "operator";
                    }
                    if (!isNaN(Number(char))) { // si el caracter actual es un numero
                        if (num1 == "") { // si no hay primer numero aún
                            num1 += char;
                            opened = true;
                        }
                        else { // numero1 no está vacio (1)
                            if (lastType == "number") { //si el ultimo caracter fue un numero
                                if (num2 == "") // si el numero 2 esta vacio aun
                                    num1 += char;
                                else // quiere decir que estoy armando el numero 2
                                    num2 += char;
                            }
                            if (lastType == "operator") { // si el ultimo caracter fue un operador
                                num2 += char;
                            }
                        }
                        lastType = "number";
                    }
                    if (key == characters.length - 1) { // si el caracter actual es el ultimo
                        closed = true;
                    }
                    else {
                        let next = characters[key + 1];
                        if (next == ')')
                            sepClosed = true;
                        if ((this.operators.includes(next) || this.separators.includes(next)) && num2 != "") {
                            closed = true;
                        }
                        if (num2 == "" && this.separators.includes(next)) { //valida si el numero1 está solo (no tiene operación inmediata)
                            num1 = "";
                            num2 = "";
                            op = "";
                            lastType = "";
                            opened = false;
                            closed = false;
                            sepOpened = false;
                            sepClosed = false;
                            return;
                        }
                    }
                    if (num1 && op && num2 && closed) {
                        let operation = num1 + op + num2;
                        let result = this.operate(num1, op, num2).toString();
                        if (sepOpened && sepClosed)
                            operation = "(" + operation + ")";
                        str = str.replace(operation, result);
                        num1 = "";
                        num2 = "";
                        op = "";
                        lastType = "";
                        opened = false;
                        closed = false;
                        sepOpened = false;
                        sepClosed = false;
                        return;
                    }
                    else {
                        if (num1 && isFactorial) {
                            let operation = num1 + op;
                            let result = this.operate(num1, op, null).toString();
                            if (sepOpened && sepClosed)
                                operation = "(" + operation + ")";
                            str = str.replace(operation, result);
                            num1 = "";
                            num2 = "";
                            op = "";
                            lastType = "";
                            opened = false;
                            closed = false;
                            sepOpened = false;
                            sepClosed = false;
                            isFactorial = false;
                            return;
                        }
                        else {
                            closed = false;
                        }
                    }
                });
            }
            return str;
        }
        return;
    }
    isOperable(str) {
        let characters = str.split("");
        let result = false;
        characters.forEach((char) => {
            //if( this.operators.includes(char) || this.separators.includes(char) ) {
            if (this.operators.includes(char)) {
                return result = true;
            }
        });
        return result;
    }
    operate(num1, operator, num2) {
        switch (operator) {
            case '+':
                try {
                    return parseFloat(num1) + parseFloat(num2);
                }
                catch (error) {
                    throw error;
                }
            case '-':
                try {
                    return parseFloat(num1) - parseFloat(num2);
                }
                catch (error) {
                    throw error;
                }
            case '*':
                try {
                    return parseFloat(num1) * parseFloat(num2);
                }
                catch (error) {
                    throw error;
                }
            case '/':
                try {
                    return parseFloat(num1) / parseFloat(num2);
                }
                catch (error) {
                    throw error;
                }
            case '**':
                try {
                    return Math.pow(parseFloat(num1), parseFloat(num2));
                }
                catch (error) {
                    throw error;
                }
            case '!':
                try {
                    let number = num1.replace("!", "");
                    return this.getFactorial(number);
                }
                catch (error) {
                    throw error;
                }
        }
        return 0;
    }
    getFactorial(number) {
        var result = 1;
        for (let i = 1; i <= number; i++) {
            result = result * i;
        }
        return result;
    }
    sanitizer(expression, context) {
        let result = "";
        expression = expression.replace(/ /g, "");
        if (context) {
            result = this.replaceVariables(expression, context);
        }
        else {
            result = expression;
        }
        return result;
    }
    validString(characters) {
        let valid = true;
        characters.forEach(char => {
            if (!this.validCharacter(char)) {
                return valid = false;
            }
        });
        return valid;
    }
    validCharacter(char) {
        if (this.separators.includes(char))
            return true;
        if (this.operators.includes(char))
            return true;
        if (!isNaN(Number(char)))
            return true;
        if (char == ".")
            return true;
        return false;
    }
    replaceVariables(expression, context) {
        let variables = Object.keys(context);
        variables.forEach(variable => {
            let value = context[variable];
            let term = new RegExp(variable, 'g');
            expression = expression.replace(term, value);
        });
        return expression;
    }
}
exports.arithmetic = new Arithmetic();
