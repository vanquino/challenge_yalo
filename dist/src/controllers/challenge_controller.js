"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.challengeController = void 0;
const arithmetic_1 = require("./../lib/arithmetic");
const logic_1 = require("./../lib/logic");
class ChallengeController {
    evaluationArithmetic(req, res) {
        let response = {};
        let body = req.body;
        let expression = body.expression;
        let context = body.context;
        let next = body.transitions.next;
        let error = body.transitions.error;
        let save = body.save;
        let result;
        try {
            result = arithmetic_1.arithmetic.evaluate(expression, context);
            if (isNaN(Number(result))) {
                response[save] = "NaN";
                response.transition = error;
            }
            else {
                if (Number(result) % 1 == 0) {
                    response[save] = result;
                    response.transition = next;
                }
                else {
                    response[save] = Number(result).toFixed(11).toString();
                    response.transition = next;
                }
            }
        }
        catch (e) {
            response[save] = "NaN";
            response.transition = error;
        }
        res.send(response);
    }
    evaluationLogical(req, res) {
        let response = {};
        let body = req.body;
        let expression = body.expression;
        let context = body.context;
        let save = body.save;
        let isTrue = body.transitions.isTrue;
        let isFalse = body.transitions.isFalse;
        let isError = body.transitions.isError;
        let result;
        try {
            result = logic_1.logic.evaluate(expression, context);
            if (result == "true") {
                response[save] = true;
                response.transition = isTrue;
            }
            if (result == "false") {
                response[save] = false;
                response.transition = isFalse;
            }
        }
        catch (e) {
            response[save] = e.message;
            response.transition = isError;
        }
        res.send(response);
    }
}
exports.challengeController = new ChallengeController();
