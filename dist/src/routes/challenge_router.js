"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const challenge_controller_1 = require("../controllers/challenge_controller");
const router = express_1.Router();
router.get('/arithmetic', (req, res) => {
    challenge_controller_1.challengeController.evaluationArithmetic(req, res);
});
router.get('/logic', (req, res) => {
    challenge_controller_1.challengeController.evaluationLogical(req, res);
});
exports.default = router;
