import express from 'express';
import challenge  from './routes/challenge_router';

const app = express();
// settings
app.set('port', process.env.PORT || 3000);

// middlewares
app.use(express.json());
app.use(express.urlencoded({extended:false}));

// routes
app.use('/evaluation',challenge);

app.listen(app.get('port'), () => {
    console.log(`Server on port ${app.get('port')}`);
});

module.exports = app;