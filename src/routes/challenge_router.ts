import { Router } from 'express';
import { challengeController } from '../controllers/challenge_controller';

const router : Router = Router();

router.get('/arithmetic', (req, res) => {
    challengeController.evaluationArithmetic(req,res)
});

router.get('/logic', (req, res) => {
    challengeController.evaluationLogical(req,res)
});

export default router;