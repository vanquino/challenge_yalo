const app = require("./../src/index");
const assert = require('assert');
const request = require('supertest');

describe('Arithmetic Expressions Testing', () => {
    it('evaluate expression value/(99**2) with value = 180 using power', done => {
        let body = {
            "expression": "value/(99**2)",
            "save": "result",
            "transitions": {
                "next": 1,
                "error": 2
            },
            "context": {
                "value": 180
            }
        };

        let expected = {
            "result": "0.01836547291",
            "transition": 1
        };

        request(app)
            .get('/evaluation/arithmetic')
            .set('Content-Type', 'application/json')
            .send(body)
            .end( ( error:any, response:any ) => {
                let result = response.body.result;
                let transition = response.body.transition;
                assert((result == expected.result) && (transition == expected.transition));
                if(error)
                    console.log(error);
                done();
            })
    });

    it('evaluate expression variable*(30/3) with variable = 20', done => {
        let body = {
            "expression": "variable*(30/3)",
            "save": "res",
            "transitions": {
                "next": 1,
                "error": 2
            },
            "context": {
                "variable": 20
            }
        };

        let expected : any = {
            "res": "200",
            "transition": 1
        };

        request(app)
            .get('/evaluation/arithmetic')
            .set('Content-Type', 'application/json')
            .send(body)
            .end( ( error:any, response:any ) => {
                let result = response.body.res;
                let transition = response.body.transition;
                assert( (result == expected.res) && (transition == expected.transition) );
                if(error)
                    console.log(error);
                done();
            });
    });

    it('evaluate expression 527-(number*variable)  with number = 15 and variable = 4', done => {
        let body = {
            "expression": "527-(number*variable)",
            "save": "operation",
            "transitions": {
                "next": 25,
                "error": 40
            },
            "context": {
                "number": 15,
                "variable": 4
            }
        };

        let expected : any = {
            "operation": "467",
            "transition": 25
        };

        request(app)
            .get('/evaluation/arithmetic')
            .set('Content-Type', 'application/json')
            .send(body)
            .end( ( error:any, response:any ) => {
                let result = response.body.operation;
                let transition = response.body.transition;
                assert( (result == expected.operation) && (transition == expected.transition) );
                if(error)
                    console.log(error);
                done();
            });
    });

    it('evaluate expression (str/2) with str = string-value', done => {
        let body = {
            "expression": "(str/2)",
            "save": "result",
            "transitions": {
                "next": 101,
                "error": 102
            },
            "context": {
                "str": "string-value"
            }
        };

        let expected = {
            "result": "NaN",
            "transition": 102
        };

        request(app)
            .get('/evaluation/arithmetic')
            .set('Content-Type', 'application/json')
            .send(body)
            .end( ( error:any, response:any ) => {
                let result = response.body.result;
                let transition = response.body.transition;
                assert((result == expected.result) && (transition == expected.transition));
                if(error)
                    console.log(error);
                done();
            })
    });

    it('evaluate expression (10/2)', done => {
        let body = {
            "expression": "(10/2)",
            "save": "result",
            "transitions": {
                "next": 25,
                "error": 50
            },
            "context": {}
        };

        let expected = {
            "result": "5",
            "transition": 25
        };

        request(app)
            .get('/evaluation/arithmetic')
            .set('Content-Type', 'application/json')
            .send(body)
            .end( ( error:any, response:any ) => {
                let result = response.body.result;
                let transition = response.body.transition;
                assert((result == expected.result) && (transition == expected.transition));
                if(error)
                    console.log(error);
                done();
            })
    });

    it('evaluate expression 18 + 62', done => {
        let body = {
            "expression": " 18 + 62",
            "save": "adding",
            "transitions": {
                "next": 33,
                "error": 87
            }
        };

        let expected = {
            "adding": "80",
            "transition": 33
        };

        request(app)
            .get('/evaluation/arithmetic')
            .set('Content-Type', 'application/json')
            .send(body)
            .end( ( error:any, response:any ) => {
                let result = response.body.adding;
                let transition = response.body.transition;
                assert((result == expected.adding) && (transition == expected.transition));
                if(error)
                    console.log(error);
                done();
            })
    });

    it('evaluate expression (5!+7-(3*9)) using factorial', done => {
        let body = {
            "expression": "(5!+7-(3*9))",
            "save": "result",
            "transitions": {
                "next": 1,
                "error": 0
            }
        };

        let expected = {
            "result": "100",
            "transition": 1
        };

        request(app)
            .get('/evaluation/arithmetic')
            .set('Content-Type', 'application/json')
            .send(body)
            .end( ( error:any, response:any ) => {
                let result = response.body.result;
                let transition = response.body.transition;
                assert( ( result == expected.result ) && ( transition == expected.transition ) );
                if(error)
                    console.log(error);
                done();
            })
    });
});

describe('Logic Expressions Testing', () => {
    it('evaluate expression (5 > 10)', done => {
        let body = {
            "expression": "(5 > 10)",
            "save": "adult",
            "transitions": {
                "isTrue": 15,
                "isFalse": 23,
                "isError": 45
            },
            "context": {
                "age": 15
            }
        };

        let expected = {
            "adult": false,
            "transition": 23
        };

        request(app)
            .get('/evaluation/logic')
            .set('Content-Type', 'application/json')
            .send(body)
            .end( ( error:any, response:any ) => {
                let result = response.body.adult;
                let transition = response.body.transition;
                assert((result == expected.adult) && (transition == expected.transition));
                if(error)
                    console.log(error);
                done();
            })
    });

    it('evaluate expression (4 == 4)', done => {
        let body = {
            "expression": "(4 == 4)",
            "save": "compare",
            "transitions": {
                "isTrue": 5,
                "isFalse": 2,
                "isError": 4
            },
            "context": {
                "age": 15
            }
        };

        let expected = {
            "compare": true,
            "transition": 5
        };

        request(app)
            .get('/evaluation/logic')
            .set('Content-Type', 'application/json')
            .send(body)
            .end( ( error:any, response:any ) => {
                let result = response.body.compare;
                let transition = response.body.transition;
                assert((result == expected.compare) && (transition == expected.transition));
                if(error)
                    console.log(error);
                done();
            })
    });

    it('evaluate expression (10 < 20)', done => {
        let body = {
            "expression": "(10 < 20)",
            "save": "compare",
            "transitions": {
                "isTrue": 2,
                "isFalse": 2,
                "isError": 4
            },
            "context": {
                "age": 15
            }
        };

        let expected = {
            "compare": true,
            "transition": 2
        };

        request(app)
            .get('/evaluation/logic')
            .set('Content-Type', 'application/json')
            .send(body)
            .end( ( error:any, response:any ) => {
                let result = response.body.compare;
                let transition = response.body.transition;
                assert((result == expected.compare) && (transition == expected.transition));
                if(error)
                    console.log(error);
                done();
            })
    });

    it('evaluate expression (4 != 90)', done => {
        let body = {
            "expression": "(4 < 90)",
            "save": "compare",
            "transitions": {
                "isTrue": 2,
                "isFalse": 2,
                "isError": 4
            },
            "context": {
                "age": 15
            }
        };

        let expected = {
            "compare": true,
            "transition": 2
        };

        request(app)
            .get('/evaluation/logic')
            .set('Content-Type', 'application/json')
            .send(body)
            .end( ( error:any, response:any ) => {
                let result = response.body.compare;
                let transition = response.body.transition;
                assert((result == expected.compare) && (transition == expected.transition));
                if(error)
                    console.log(error);
                done();
            })
    });

    it('evaluate expression (8 <= 32)', done => {
        let body = {
            "expression": "(8 <= 32)",
            "save": "compare",
            "transitions": {
                "isTrue": 2,
                "isFalse": 2,
                "isError": 4
            },
            "context": {
                "age": 15
            }
        };

        let expected = {
            "compare": true,
            "transition": 2
        };

        request(app)
            .get('/evaluation/logic')
            .set('Content-Type', 'application/json')
            .send(body)
            .end( ( error:any, response:any ) => {
                let result = response.body.compare;
                let transition = response.body.transition;
                assert((result == expected.compare) && (transition == expected.transition));
                if(error)
                    console.log(error);
                done();
            })
    });

    it('evaluate expression (32 <= 32)', done => {
        let body = {
            "expression": "(32 <= 32)",
            "save": "compare",
            "transitions": {
                "isTrue": 2,
                "isFalse": 2,
                "isError": 4
            },
            "context": {
                "age": 15
            }
        };

        let expected = {
            "compare": true,
            "transition": 2
        };

        request(app)
            .get('/evaluation/logic')
            .set('Content-Type', 'application/json')
            .send(body)
            .end( ( error:any, response:any ) => {
                let result = response.body.compare;
                let transition = response.body.transition;
                assert((result == expected.compare) && (transition == expected.transition));
                if(error)
                    console.log(error);
                done();
            })
    });

    it('evaluate expression (10 >= 20)', done => {
        let body = {
            "expression": "(10 >= 20)",
            "save": "compare",
            "transitions": {
                "isTrue": 2,
                "isFalse": 2,
                "isError": 4
            },
            "context": {
                "age": 15
            }
        };

        let expected = {
            "compare": false,
            "transition": 2
        };

        request(app)
            .get('/evaluation/logic')
            .set('Content-Type', 'application/json')
            .send(body)
            .end( ( error:any, response:any ) => {
                let result = response.body.compare;
                let transition = response.body.transition;
                assert((result == expected.compare) && (transition == expected.transition));
                if(error)
                    console.log(error);
                done();
            })
    });

    it('evaluate expression ( ( 10 > 2 ) || ( 20 < 5 ) )', done => {
        let body = {
            "expression": "( ( 10 > 2 ) || ( 20 < 5 ) )",
            "save": "compare",
            "transitions": {
                "isTrue": 2,
                "isFalse": 2,
                "isError": 4
            },
            "context": {
                "age": 15
            }
        };

        let expected = {
            "compare": true,
            "transition": 2
        };

        request(app)
            .get('/evaluation/logic')
            .set('Content-Type', 'application/json')
            .send(body)
            .end( ( error:any, response:any ) => {
                let result = response.body.compare;
                let transition = response.body.transition;
                assert((result == expected.compare) && (transition == expected.transition));
                if(error)
                    console.log(error);
                done();
            })
    });

    it('evaluate expression ( ( 10 > 2 ) && ( 20 < 5 ) )', done => {
        let body = {
            "expression": "( ( 10 > 2 ) && ( 20 < 5 ) )",
            "save": "compare",
            "transitions": {
                "isTrue": 2,
                "isFalse": 2,
                "isError": 4
            },
            "context": {
                "age": 15
            }
        };

        let expected = {
            "compare": false,
            "transition": 2
        };

        request(app)
            .get('/evaluation/logic')
            .set('Content-Type', 'application/json')
            .send(body)
            .end( ( error:any, response:any ) => {
                let result = response.body.compare;
                let transition = response.body.transition;
                assert((result == expected.compare) && (transition == expected.transition));
                if(error)
                    console.log(error);
                done();
            })
    });

    it('evaluate expression ( ( 10 == 2 ) || ( 20 != 5 ) )', done => {
        let body = {
            "expression": "( ( 10 == 2 ) || ( 20 != 5 ) )",
            "save": "compare",
            "transitions": {
                "isTrue": 2,
                "isFalse": 2,
                "isError": 4
            },
            "context": {
                "age": 15
            }
        };

        let expected = {
            "compare": false,
            "transition": 2
        };

        request(app)
            .get('/evaluation/logic')
            .set('Content-Type', 'application/json')
            .send(body)
            .end( ( error:any, response:any ) => {
                let result = response.body.compare;
                let transition = response.body.transition;
                assert((result == expected.compare) && (transition == expected.transition));
                if(error)
                    console.log(error);
                done();
            })
    });
});