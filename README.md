# challenge_yalo

¡Hola a todos!


para correr el challenge se necesita:

 - Tener instalado y configurado el ambiente de node js
 - descargar el repositorio
 - abrir la terminar y navegar a la carpeta del proyecto
 - ejecutar npm install
 - ejecutar npm run build
 - ejecutar npm run dev

Para correr las pruebas se necesita
- ejecutar npm test en la terminal

y eso es todo.

Gracias por la oportunidad :)